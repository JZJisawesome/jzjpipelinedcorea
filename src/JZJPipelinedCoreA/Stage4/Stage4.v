module Stage4
(
	input clock,
	input reset,
	
	//Memory Connections
	output [29:0] memoryAddress,
	input [31:0] memoryDataIn,
	
	//Inputs
	input [31:0] instructionIn,
	input instructionIsValidIn,
	input [31:0] pcOfInstructionIn,
	input [31:0] rs2In,
	input [31:0] executeResultIn,
	input useResultAsNextPCIn,
	
	//Outputs
	output reg [31:0] instructionOut,
	output reg instructionIsValidOut = 1'b0,
	output reg [31:0] pcOfInstructionOut,
	output reg [31:0] nextPCOut,//Might be equal to nextSeqPC or could be something else
	output reg [31:0] nextSeqPCOut,//Saved to rd for jalr instructions
	output reg [31:0] resultOut,//Result directly from execute stage, potentially to be written in stage5 (could also be memory address)
	output [31:0] memoryOut,//Data from memory at the address of resultOut, potentially to be written in stage5
	output reg [31:0] rs2Out//Might also be written in stage5
);
/* Wires and Assignments */

reg [31:0] nextPC;
wire [31:0] nextSeqPC = pcOfInstructionIn + 4;

assign memoryAddress = executeResultIn[31:2];//If the instruction is a memory access, this will be the physical address
assign memoryOut = memoryDataIn;//Memory reads occur on posedge, so we pass through combinationally
//Modification of data from memory for storage in registers or back to memory occurs in stage5

/* Output Latching Logic */

//Some things are just passed through
always @(posedge clock, posedge reset)
begin
	if (reset)
	begin
		instructionOut <= 32'h00000000;
		instructionIsValidOut <= 1'b0;
		pcOfInstructionOut <= 32'h00000000;
		nextPCOut <= 32'h00000000;
		nextSeqPCOut <= 32'h00000000;
		resultOut <= 32'h00000000;
		rs2Out <= 32'h00000000;
	end
	else if (clock)
	begin
		instructionOut <= instructionIn;//Passthrough
		instructionIsValidOut <= instructionIsValidIn;//Passthrough
		pcOfInstructionOut <= pcOfInstructionIn;//Passthrough
		nextPCOut <= nextPC;
		nextSeqPCOut <= nextSeqPC;
		resultOut <= executeResultIn;//Passthrough
		rs2Out <= rs2In;//Passthrough
	end
end

/* nextPC Multiplexer */

always @*
begin
	if (useResultAsNextPCIn)
		nextPC = executeResultIn;//Jump to address calculated by alu
	else
		nextPC = nextSeqPC;
end

endmodule