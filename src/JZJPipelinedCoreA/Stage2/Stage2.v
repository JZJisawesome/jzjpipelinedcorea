module Stage2
(
	input clock,
	input reset,
	
	//Register File Connections
	output [4:0] rs1Address,
	output [4:0] rs2Address,
	input [31:0] rs1In,
	input [31:0] rs2In,
	
	//Inputs
	input [31:0] pcOfInstructionIn,
	input [31:0] instructionIn,
	input instructionIsValidIn,
	
	//Outputs
	output reg [31:0] instructionOut,
	output reg instructionIsValidOut = 1'b0,
	output reg [31:0] pcOfInstructionOut,
	output reg [31:0] rs1Out,
	output reg [31:0] rs2Out
);
/* Assignments */

//If an instruction does not have an rs1 or rs2 address, it dosen't matter
//because later stages will just ignore rs1Out and rs2Out
//This simplifies this stage significantly
assign rs1Address = instructionIn[19:15];
assign rs2Address = instructionIn[24:20];

/* Output Register Latching Logic */

//Most things are just passed through
//The only additions from this module are rs1 and rs2
always @(posedge clock, posedge reset)
begin
	if (reset)
	begin
		instructionOut <= 32'h00000000;
		instructionIsValidOut <= 1'b0;
		pcOfInstructionOut <= 32'h00000000;
		rs1Out <= 32'h00000000;
		rs2Out <= 32'h00000000;
	end
	else if (clock)
	begin
		instructionOut <= instructionIn;//Passthrough
		instructionIsValidOut <= instructionIsValidIn;//Passthrough
		pcOfInstructionOut <= pcOfInstructionIn;//Passthrough
		
		rs1Out <= rs1In;//Latch async output from register file
		rs2Out <= rs2In;//Latch async output from register file
	end
end

endmodule