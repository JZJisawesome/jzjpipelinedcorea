module RegisterFileMux//Purely combinational
(
	input [6:0] opcode,
	input [31:0] nextSeqPC,
	input [31:0] executeResult,
	input [31:0] memoryData,
	
	output reg [31:0] registerFileIn
);
/* Multiplexer */

always @*
begin
	case (opcode)
		7'b0110111: registerFileIn = executeResult;//lui
		7'b0010111: registerFileIn = executeResult;//auipc
		7'b1101111: registerFileIn = nextSeqPC;//jal
		7'b1100111: registerFileIn = nextSeqPC;//jalr
		7'b0000011: registerFileIn = memoryData;//memory load instructions
		7'b0010011: registerFileIn = executeResult;//alu immediate instructions
		7'b0110011: registerFileIn = executeResult;//alu register-register instructions
		default: registerFileIn = 32'hxxxxxxxx;//Invalid instruction or instruction does not write to registers
	endcase
end

endmodule