module Stage5
(
	input clock,
	input reset,
	
	//Inputs
	input [31:0] instructionIn,
	input instructionIsValidIn,
	input [31:0] pcOfInstructionIn,
	input [31:0] nextSeqPCIn,//Saved to rd for jalr instructions
	input [31:0] resultIn,//Result directly from execute stage, potentially to be written in stage5 (could also be memory address of read data or data to be written)
	input [31:0] memoryIn,//Data from memory at the address of resultOut, potentially to be written in stage5
	input [31:0] rs2In,//Might also be written in stage5
	
	//Memory connections
	output [29:0] memoryAddress,
	output memoryWE,
	output [31:0] memoryDataToWrite,
	
	//Register file connections
	output [4:0] rdAddress,
	output [31:0] rd,
	output rdWE
);
/* Wires and Assignments */

wire [6:0] opcode = instructionIn[6:0];
wire [2:0] funct3 = instructionIn[14:12];
wire [31:0] modifiedMemoryData;//Sign extension, etc

reg rdInstructionWE;//Instruction says to write to register
wire memoryInstructionWE;//Instruction says to write to memory
assign rdWE = instructionIsValidIn & rdInstructionWE;
assign memoryWE = instructionIsValidIn & memoryInstructionWE;

assign memoryAddress = resultIn[31:2];//Offset handled by memory store modification module
assign rdAddress = instructionIn[11:7];

/* Write Enable Logic */

assign memoryInstructionWE = opcode == 7'b0100011;

always @*
begin
	case (opcode)
		7'b0110111: rdInstructionWE = 1'b1;//lui
		7'b0010111: rdInstructionWE = 1'b1;//auipc
		7'b1101111: rdInstructionWE = 1'b1;//jal
		7'b1100111: rdInstructionWE = 1'b1;//jalr
		7'b0000011: rdInstructionWE = 1'b1;//load instructions
		7'b0010011: rdInstructionWE = 1'b1;//alu immediate instructions
		7'b0110011: rdInstructionWE = 1'b1;//alu register-register instructions
		default: rdInstructionWE = 1'b0;
	endcase
end

/* External Modules */

RegisterFileMux registerFileMux(.opcode(opcode), .nextSeqPC(nextSeqPCIn), .executeResult(resultIn), .memoryData(modifiedMemoryData), .registerFileIn(rd));

MemoryLoadModifier memoryLoadModifier(.funct3(funct3), .addressOffset(resultIn[1:0]), .rawDataIn(memoryIn), .registerDataOut(modifiedMemoryData));

MemoryStoreModifier memoryStoreModifier(.funct3(funct3), .addressOffset(resultIn[1:0]), .dataCurrentlyAtAddress(memoryIn), .rs2(rs2In), .dataToWrite(memoryDataToWrite));

endmodule