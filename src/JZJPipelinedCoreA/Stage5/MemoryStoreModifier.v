module MemoryStoreModifier
(
	input [2:0] funct3,//Assumes instruction is a memory load
	input [1:0] addressOffset,
	input [31:0] dataCurrentlyAtAddress,
	input [31:0] rs2,
	output reg [31:0] dataToWrite
);
/* Logic */

//todo
always @*
begin
	case (funct3)
		3'b000: dataToWrite = replaceByteAtOffset(dataCurrentlyAtAddress, rs2[7:0], addressOffset);
		3'b001: dataToWrite = replaceHWAtOffset(dataCurrentlyAtAddress, toLittleEndian16(rs2[15:0]), addressOffset);
		3'b010: dataToWrite = toLittleEndian(rs2);//rs2 does not require modification
		default: dataToWrite = 32'hxxxxxxxx;//Bad funct3 or different instruction
	endcase
end

/* Byte And Half Word Modification Functions */

function automatic [31:0] replaceByteAtOffset(input [31:0] word, input [7:0] newByte, input [1:0] offset);
begin
	case (offset)
		2'b00: replaceByteAtOffset = {newByte, word[23:0]};
		2'b01: replaceByteAtOffset = {word[31:24], newByte, word[15:0]};
		2'b10: replaceByteAtOffset = {word[31:16], newByte, word[7:0]};
		2'b11: replaceByteAtOffset = {word[31:8], newByte};
	endcase
end
endfunction

function automatic [31:0] replaceHWAtOffset(input [31:0] word, input [15:0] newHW, input [1:0] offset);
begin
	case (offset)
		2'b00: replaceHWAtOffset = {newHW, word[15:0]};
		2'b01: replaceHWAtOffset = 32'hxxxxxxxx;//invalid: not aligned
		2'b10: replaceHWAtOffset = {word[31:16], newHW};
		2'b11: replaceHWAtOffset = 32'hxxxxxxxx;//invalid: not aligned
	endcase
end
endfunction

/* External Stuff */

//Endianness functions
`include "JZJPipelinedCoreA/Memory/EndiannessFunctions.v"

endmodule