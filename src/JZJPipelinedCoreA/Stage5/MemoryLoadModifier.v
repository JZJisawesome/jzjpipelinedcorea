module MemoryLoadModifier//Purely combinational
(
	input [2:0] funct3,//Assumes instruction is a memory load
	input [1:0] addressOffset,
	input [31:0] rawDataIn,
	output reg [31:0] registerDataOut
);
/* Logic */

always @*
begin
	case (funct3)
		3'b000: registerDataOut = signExtend8To32(getByteAtOffset(rawDataIn, addressOffset));//lb
		3'b001: registerDataOut = signExtend16To32(toBigEndian16(getHWAtOffset(rawDataIn, addressOffset)));//lh
		3'b010: registerDataOut = toBigEndian(rawDataIn);//lw
		3'b100: registerDataOut = zeroExtend8To32(getByteAtOffset(rawDataIn, addressOffset));//lbu
		3'b101: registerDataOut = zeroExtend16To32(toBigEndian16(getHWAtOffset(rawDataIn, addressOffset)));//lhu
		default: registerDataOut = 32'hxxxxxxxx;//Bad funct3 or different instruction
	endcase
end

/* Byte And Half Word Selection Functions */

function automatic [7:0] getByteAtOffset(input [31:0] word, input [1:0] offset);
begin
	case (offset)
		2'b00: getByteAtOffset = word[31:24];
		2'b01: getByteAtOffset = word[23:16];
		2'b10: getByteAtOffset = word[15:8];
		2'b11: getByteAtOffset = word[7:0];
	endcase
end
endfunction

function automatic [15:0] getHWAtOffset(input [31:0] word, input [1:0] offset);
begin
	case (offset)
		2'b00: getHWAtOffset = word[31:16];
		2'b01: getHWAtOffset = 16'hxxxx;//invalid: not aligned
		2'b10: getHWAtOffset = word[15:0];
		2'b11: getHWAtOffset = 16'hxxxx;//invalid: not aligned
	endcase
end
endfunction

/* External Stuff */

//Endianness functions
`include "JZJPipelinedCoreA/Memory/EndiannessFunctions.v"

//Bit extension functions
`include "JZJPipelinedCoreA/BitExtensionFunctions.v"

endmodule 
