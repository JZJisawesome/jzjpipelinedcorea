//Multiplexes ALU inputs as well as controls aluForceAdd, aluMod, and aluZeroLSB
module ALUMux//Purely combinational
(
	input [31:0] instruction,
	
	//Possible values for alu inputs
	input [31:0] pcOfInstruction,
	input [31:0] rs1,
	input [31:0] immediateI,
	input [31:0] immediateS,
	input [31:0] immediateB,
	input [31:0] immediateU,
	input [31:0] immediateJ,
	input [31:0] rs2,
	
	//Alu controls and inputs
	output reg [31:0] aluX,
	output reg [31:0] aluY,
	output aluForceAdd,
	output reg aluMod,
	output aluZeroLSB//For jalr
);
/* Wires */

wire [2:0] funct3 = instruction[14:12];
wire [6:0] opcode = instruction[6:0];

/* Multiplexing */

//Input X
always @*
begin
	case (opcode)
		7'b0110111: aluX = 32'h00000000;//lui
		7'b0010111: aluX = pcOfInstruction;//auipc
		7'b1101111: aluX = pcOfInstruction;//jal
		7'b1100111: aluX = rs1;//jalr
		7'b1100011: aluX = pcOfInstruction;//branch instructions
		7'b0000011: aluX = rs1;//load instructions
		7'b0100011: aluX = rs1;//store instructions
		7'b0010011: aluX = rs1;//immediate computation instructions
		7'b0110011: aluX = rs1;//register computation instructions
		default: aluX = 32'hxxxxxxxx;//Instruction does not use the alu
	endcase
end

//Input Y
always @*
begin
	case (opcode)
		7'b0110111: aluY = immediateU;//lui
		7'b0010111: aluY = immediateU;//auipc
		7'b1101111: aluY = immediateJ;//jal
		7'b1100111: aluY = immediateI;//jalr
		7'b1100011: aluY = immediateB;//branch instructions
		7'b0000011: aluY = immediateI;//load instructions
		7'b0100011: aluY = immediateS;//store instructions
		7'b0010011: aluY = immediateI;//immediate computation instructions
		7'b0110011: aluY = rs2;//register computation instructions
		default: aluY = 32'hxxxxxxxx;//Instruction does not use the alu
	endcase
end

/* Control Signals */

assign aluForceAdd = (opcode != 7'b0010011) & (opcode != 7'b0110011);//All non computative instructions either use addition (even if just to pass through an immediate) or ignore the result

//Whether to enable aluMod
always @*
begin
	if ((opcode == 7'b0010011) & (funct3 != 3'b101))//Bit 30 makes up part of the immediate value of this type of instruction and so must be ignored unless the operation is srli/srai
		aluMod = 1'b0;
	else
		aluMod = instruction[30];//Pass through bit 30 of instruction (for other instructions with immediate values, this is ignored so we don't need extra cases for that)
end

assign aluZeroLSB = opcode == 7'b1100111;//for jalr

endmodule
