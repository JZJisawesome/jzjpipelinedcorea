module ImmediateFormer//Purely combinational
(
	input [31:0] instruction,
	
	output [31:0] immediateI,
	output [31:0] immediateS,
	output [31:0] immediateB,
	output [31:0] immediateU,
	output [31:0] immediateJ
);
/* Wires and Assignments */
wire [11:0] immediateILow12Bits = instruction[31:20];
assign immediateI = signExtend12To32(immediateILow12Bits);

wire [11:0] immediateSLow12Bits = {instruction[31:25], instruction[11:7]};
assign immediateS = signExtend12To32(immediateSLow12Bits);

wire [12:0] immediateBLow13Bits = {instruction[31], instruction[7], instruction[30:25], instruction[11:8], 1'b0};//0 out bottom bit because only multiples of 2 are allowed
assign immediateB = signExtend13To32(immediateBLow13Bits);

assign immediateU[31:12] = instruction[31:12];
assign immediateU[11:0] = 12'b000000000000;//both lui and auipc zero out the bottom 12 bits, so we do that here

wire [20:0] immediateJLow21Bits = {instruction[31], instruction[19:12], instruction[20], instruction[30:21], 1'b0};//0 out bottom bit because only multiples of 2 are allowed
assign immediateJ = signExtend21To32(immediateJLow21Bits);

/* External Stuff */

//Bit extension functions
`include "JZJPipelinedCoreA/BitExtensionFunctions.v"

endmodule 
