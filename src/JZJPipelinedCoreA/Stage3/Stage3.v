module Stage3
(
	input clock,
	input reset,
	
	//Inputs
	input [31:0] instructionIn,
	input instructionIsValidIn,
	input [31:0] pcOfInstructionIn,
	input [31:0] rs1In,
	input [31:0] rs2In,
	
	//Outputs
	output reg [31:0] instructionOut,
	output reg instructionIsValidOut = 1'b0,
	output reg [31:0] pcOfInstructionOut,
	output reg [31:0] resultOut,
	output reg [31:0] rs2Out,
	output reg useResultAsNextPC//aka branch taken; for jalr also means to store pc + 4 in rd
);
/* Wires */

wire [2:0] funct3 = instructionIn[14:12];
wire [6:0] opcode = instructionIn[6:0];

//Outputs to be latched
wire branchComparatorOutput;
wire [31:0] aluResult;

//ALU multiplexing and control
wire [31:0] aluX;
wire [31:0] aluY;
wire aluForceAdd;
wire aluMod;
wire aluZeroLSB;

//Branch comparator control
wire isBranchInstruction = opcode == 7'b1100011;//conditional branch instruction
wire isUnconditionalBranch = (opcode == 7'b1101111) | (opcode == 7'b1100111);//jal/jalr

//Immediates
wire [31:0] immediateI;
wire [31:0] immediateS;
wire [31:0] immediateB;
wire [31:0] immediateU;
wire [31:0] immediateJ;

/* Output Latching Logic */

//Some things are just passed through, some are not
always @(posedge clock, posedge reset)
begin
	if (reset)
	begin
		instructionOut <= 32'h00000000;
		instructionIsValidOut <= 1'b0;
		pcOfInstructionOut <= 32'h00000000;
		resultOut <= 32'h00000000;
		rs2Out <= 32'h00000000;
		useResultAsNextPC <= 1'b0;
	end
	else if (clock)
	begin
		instructionOut <= instructionIn;//Passthrough
		instructionIsValidOut <= instructionIsValidIn;//Passthrough
		pcOfInstructionOut <= pcOfInstructionIn;//Passthrough
		resultOut <= aluResult;
		rs2Out <= rs2In;//Passthrough
		useResultAsNextPC <= branchComparatorOutput;
	end
end

/* External Modules */

BranchComparator branchComparator(.rs1(rs1In), .rs2(rs2In), .funct3(funct3), .branchInstruction(isBranchInstruction), .unconditionalBranch(isUnconditionalBranch), 
											 .branchTaken(branchComparatorOutput));

ALU alu(.funct3(funct3), .forceAdd(aluForceAdd), .mod(aluMod), .zeroLSB(aluZeroLSB), .x(aluX), .y(aluY), .result(aluResult));

ALUMux aluMux(.instruction(instructionIn), .pcOfInstruction(pcOfInstructionIn), .rs1(rs1In), .immediateI(immediateI), .immediateS(immediateS), .immediateU(immediateU),
				  .immediateB(immediateB), .immediateJ(immediateJ), .rs2(rs2In), .aluX(aluX), .aluY(aluY), .aluForceAdd(aluForceAdd), .aluMod(aluMod), .aluZeroLSB(aluZeroLSB));

ImmediateFormer immediateFormer(.instruction(instructionIn), .immediateI(immediateI), .immediateS(immediateS), . immediateB(immediateB), .immediateU(immediateU), .immediateJ(immediateJ));

endmodule 
