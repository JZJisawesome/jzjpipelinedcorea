`timescale 1ns/1ps
module JZJPipelinedCoreA_tb;

reg clock = 1'b0;

JZJPipelinedCoreA #(.INITIAL_MEM_CONTENTS("../src/memFiles/adding2.mem")) coreTest (.clock(clock), .reset(1'b0));

always
begin
    #10;
    clock = ~clock;
end

initial
begin
    $dumpfile("/tmp/JZJPipelinedCoreA.vcd");
    $dumpvars(0,JZJPipelinedCoreA_tb);
    #10000 $finish;
end

endmodule
